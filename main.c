#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "parse.h"
#include "Action.h"


int main(int argc, char* argv[]) {
    bool header = true;

    if(argc == 1)return 1;
    //reads the file
    struct parse_container* pc = parse_file(argv[argc-1]);

    // last element of argv is reserved for filepath
    for(int i = 1; i < argc-1; i++){
        if (strcmp(argv[i], "-f") == 0) {
            /*Display the number of fields in the first record of file.*/
            printf("%i\n", pc->n_fields); 
        }
        else if (strcmp(argv[i], "-r") == 0) {
            printf("%i\n", pc->n_entries);
        }
        else if (strcmp(argv[i], "-h") == 0) {
            header = 1;
            // unused for now, using header params
            /*create_offset(pc, 1);*/
        }
        else if (strcmp(argv[i], "-max") == 0) {
            i++;
            if (i >= argc) {
                puts("Error: No field name after -max.");
                return 1;
            }
            char** field = get_field(pc, argv[i]);
            if(!field){
                printf("Error: Field %s does not exist in the file.\n", argv[i]);
                return 1;
            }
            max(header, pc->n_entries-header, field);
        }
        else if (strcmp(argv[i], "-min") == 0) {
            i++;
            if (i >= argc) {
                puts("Error: No field name after -min.");
                return 1;
            }

            char** field = get_field(pc, argv[i]);
            if(!field){
                printf("Error: Field %s does not exist in the file.\n", argv[i]);
                return 1;
            }
            min(header, pc->n_entries-header, field);
        }
        else if (strcmp(argv[i], "-mean") == 0) {
            i++;
            if (i >= argc) {
                puts("Error: No field name after -mean.");
                return 1;
            }

            char** field = get_field(pc, argv[i]);
            if(!field){
                printf("Error: Field %s does not exist in the file.\n", argv[i]);
                return 1;
            }
            mean(header, pc->n_entries-header, field);
        }
        else if (strcmp(argv[i], "-records") == 0) {
            // if this is true, the next error will be too
            // don't return, print both
            if (i+1 >= argc)
                puts("Error: No field name after -records.");
            
            if (i+2 >= argc) {
                puts("Error: No value  after -records field.");
                return 1;
            }
            records(header, argv[i+1], argv[i+2], pc);
            i += 2;
        }
        else {
            puts("Error: Incorrect arguments.");
            return 1;
        }
    }
    return 0;
}
