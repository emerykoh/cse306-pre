#include "Util.h"

// toInt : string -> int
// converts string to integer value
int toInt(char str[]) {
    int length = strlen(str);
    char zero = '0';

    //create return integer
    int ret = 0;
    _Bool isNeg = 0;
    for (int i = 0; i < length; i++) {
        char c = str[i];
        
        //flag if negative
        if (c == '-') {
          isNeg = 1;
          continue;
        }
        //some end-of-line character we need to ignore
        else if (c == 13) {
            continue;
        }

        //add digit to integer
        int digit = c - zero;
        ret *= 10;
        ret += digit;
    }

    //make negative if flagged
    if (isNeg) {
      ret *= -1;
    }

    return ret;
}

// toDouble : string -> double
// converts string to double value
double toDouble(char str[]) {
    int length = strlen(str);
    char zero = '0';
    
    //create return double
    double ret = 0;
    _Bool isNeg = 0;
    _Bool foundDecimal = 0;
    double decimalDivisor = 1;
    for (int i = 0; i < length; i++) {
        char c = str[i];
        
        //flag if negative
        if (c == '-') {
            isNeg = 1;
            continue;
        }
        //flag to start counting decimal places
        else if (c == '.') {
            foundDecimal = 1;
            continue;
        }
        //some end-of-line character we need to ignore
        else if (c == 13) {
            continue;
        }
        //count decimal places if flagged
        else if (foundDecimal) {
            decimalDivisor *= 10;
        }

        //add digit to integer
        int digit = c - zero;
        ret *= 10;
        ret += digit;
    }

    //make negative if flagged
    if (isNeg) {
      ret *= -1;
    }

    //divide by divisor
    ret /= decimalDivisor;

    return ret;
}

// getType : string -> int
// returns the type of given string
// 1, 2, or 3 for int, double, or non-numeric respectively
int getType(char recordValue[]){
    const char *intList = "-0123456789\0";
    const char *decList = ".";
    char *charP = recordValue;
    int decCount = 0;
    int recordType = 0;

    //scan over input string
    while (*charP) {
        if (strchr(decList, *charP)){
            //if first decimal (and we aren't at beginning of string), flag and continue
            //otherwise, non numeric
            if (decCount < 1 && charP != recordValue){
                recordType = 1;
                decCount += 1;
            }
            else {
                return 2;
            }
        }
        else if (!strchr(intList, *charP)) {
            if (*charP == 13 && !(*(charP + 1))) {
                return recordType;
            }
            
            //if not a decimal & not a number, it's non-numeric.
            return 2;
        }
        charP++;
    }

    return recordType;
}
