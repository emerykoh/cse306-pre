#include "parse.h"
#include "Util.h"
#include <stdio.h>

int max(_Bool header, int count, char* fieldData[]);
int min(_Bool header, int count, char* fieldData[]);
int mean(_Bool header, int count, char* fieldData[]);
int records(_Bool header, char* field, char* value, struct parse_container* pc);
