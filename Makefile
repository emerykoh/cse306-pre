CXX=gcc
all: csv
csv:
	$(CXX) main.c parse.c Util.c Action.c -o csv -Wall -Wextra -Wpedantic -O3

clean:
	rm -f csv
