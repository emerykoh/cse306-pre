#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parse.h"


struct parse_container* init_pc(struct parse_container* pc){
      if(!pc)pc = malloc(sizeof(struct parse_container));
      pc->n_fields = 0;
      pc->n_entries = 0;
      pc->cap = 10;
      pc->entries = malloc(sizeof(struct entry)*pc->cap);
      return pc;
}

// header line will never have quotations
int get_n_fields(char* ln){
      int ret = 0;
      for(char* i = ln; *i; ++i)if(*i == ',')++ret;
      // +1 for field after last comma
      return ret+1;
}

// takes in a comma separated line
// *n_fields is set to number of fields
struct entry build_entry(char* ln, int* n_fields, int llen){
      if(n_fields && *n_fields == 0){
            *n_fields = get_n_fields(ln);
      }
      struct entry e;
      e.raw_ln = ln;
      e.entry_strs = malloc(sizeof(char*)*(*n_fields));
      int ind = 0;

      _Bool in_quote = *ln == '"';

      char* beg = ln;
      for(char* i = ln; *i != 0; ++i){
            if(*i == '"')in_quote = !in_quote;
            // if we've found a comma
            if(*i == ',' && !in_quote){
                  e.entry_strs[ind] = malloc(i-beg);
                  strncpy(e.entry_strs[ind++], beg, i-beg);
                  beg = i+1;
            }
      }
      e.entry_strs[ind] = malloc(ln-beg+llen);
      strncpy(e.entry_strs[ind++], beg, ln-beg+llen);

      return e;
}

void insert_entry(struct entry e, struct parse_container* pc){
      if(pc->n_entries == pc->cap){
           pc->cap *= 2; 
           struct entry* tmp_entries = malloc(sizeof(struct entry)*pc->cap);
           memcpy(tmp_entries, pc->entries, sizeof(struct entry)*pc->n_entries);
           free(pc->entries);
           pc->entries = tmp_entries;
      }
      pc->entries[pc->n_entries++] = e;
}

struct parse_container* parse_file(char* fpath){
      FILE* fp = fopen(fpath, "r");
      if(!fp)return NULL;
      size_t sz = 0;
      char* ln = NULL;
      int n_read = 0;
      
      struct parse_container* pc = init_pc(NULL);
      while((n_read = getline(&ln, &sz, fp)) != EOF){
            ln[--n_read] = 0;
            insert_entry(build_entry(ln, &pc->n_fields, n_read), pc);
            ln = NULL;
      }
      fclose(fp);

      return pc;
}

char** get_field(struct parse_container* pc, char* field){
      if(!pc)return NULL;
      // char** will always be of size n_entries
      char** ret = malloc(sizeof(char*)*pc->n_entries);
      int i;
      for(i = 0; i < pc->n_fields; ++i)
            if(strstr(pc->entries[0].entry_strs[i], field))break;
      if(i == pc->n_fields){
            free(ret);
            return NULL;
      }
      for(int j = 0; j < pc->n_entries; ++j)
            ret[j] = pc->entries[j].entry_strs[i];
      return ret;
}

void create_offset(struct parse_container* pc, int offset){
      pc->entries += offset;
      pc->n_entries += offset;
}
