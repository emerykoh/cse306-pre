#include <stdio.h>
#include <string.h>

int getType(char str[]);
int toInt(char str[]);
double toDouble(char str[]);
