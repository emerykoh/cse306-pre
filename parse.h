/* include guard */
#ifndef PARSE_H
#define PARSE_H
struct entry{
      char* raw_ln;
      char** entry_strs;
};

struct parse_container{
      int n_entries, n_fields;
      int cap;
      struct entry* entries;
};

struct parse_container* parse_file(char* fpath);

/*
 * if field isn't found, get_field returns NULL
 * otherwise,
 * get_field returns a malloc'd char** of size pc->n_entries
 */
char** get_field(struct parse_container* pc, char* field);
void create_offset(struct parse_container* pc, int offset);


/* a simple example to show functionality of parse_container and get_field */
/*
 
int main(int argc, char** argv){
      if(argc < 3)return 1;
      struct parse_container* pc = parse_file(argv[1]);
      char** dates = get_field(pc, argv[2]);
      if(!dates){
            printf("no entry with field: %s found\n", argv[2]);
            return 1;
      }
      printf("all entries from field: %s\n", argv[2]);
      for(int i = 0; i < pc->n_entries; ++i){
            printf("entry #%i: %s\n", i, dates[i]);
      }
      return 0;
}

*/
#endif
