#include <stdlib.h> // free
#include "parse.h"
#include "Util.h"
#include "Action.h"

// max : bool int [string] -> int
// given a list of string-numbers, prints out the max value
// if the strings given are not numbers, return 0, otherwise return 1
int max(_Bool header, int count, char* fieldData[]) {
    int offset = header ? 1 : 0;

    //if there is no data to look at, fail
    if (offset >= count) {
        return 0;
    }    

    //determine type
    int type = getType(fieldData[offset]);

    //find max value
    int maxInt = 0;
    double maxDouble = 0;
    for (int i = offset; i < count + offset; i++) {
        char* s = fieldData[i];
        
        //if type inconsistent, fail
        if (getType(s) != type) {
            /*
             *printf("ERROR: field not consistent type.");
             *return 0;
             */
            continue;
        }

        if (type == 0) { //int
            //get value and check if new max
            int value = toInt(s);
            if (i == offset || value > maxInt) {
                maxInt = value;
            }
        }
        else if (type == 1) { //double
            //get value and check if new max
            double value = toDouble(s);
            if (i == offset || value > maxDouble) {
                maxDouble = value;
            }
        }
        else {//non-numeric (error)
            /*
             *printf("ERROR: the field is non-numeric.");
             *return 0;
             */
            continue;
        }
    }
    
    if (type == 0) {
        printf("%d\n", maxInt);
    }
    else {
        printf("%f\n", maxDouble);
    }
    return 1;
}

// min : bool int [string] -> int
// given a string of string-numbers, prints out the min value
// if the strings given are not numbers, return 0, otherwise return 1
int min(_Bool header, int count, char* fieldData[]) {
    int offset = header ? 1 : 0;

    //if there is no data to look at, fail
    if (offset >= count) {
        return 0;
    }    

    //determine type
    int type = getType(fieldData[offset]);

    //find max value
    int maxInt = 0;
    double maxDouble = 0;
    for (int i = offset; i < count + offset; i++) {
        char* s = fieldData[i];
        
        //if type inconsistent, fail
        if (getType(s) != type) {
            /*
             *printf("ERROR: field not consistent type.");
             *return 0;
             */
            continue;
        }

        if (type == 0) { //int
            //get value and check if new min
            int value = toInt(s);
            if (i == offset || value < maxInt) {
                maxInt = value;
            }
        }
        else if (type == 1) { //double
            //get value and check if new min
            double value = toDouble(s);
            if (i == offset || value < maxDouble) {
                maxDouble = value;
            }
        }
        else {//non-numeric (error)
            /*
             *printf("ERROR: the field is non-numeric.");
             *return 0;
             */
            continue;
        }
    }
    
    if (type == 0) {
        printf("%d\n", maxInt);
    }
    else {
        printf("%f\n", maxDouble);
    }
    return 1;
}

// mean : bool int [string] -> int
// given a string of string-numbers, prints out the average value
// if the strings given are not numbers, return 0, otherwise return 1
int mean(_Bool header, int count, char* fieldData[]) {
    int offset = header ? 1 : 0;

    //if there is no data to look at, fail
    if (offset >= count) {
        return 0;
    }    

    //determine type
    int type = getType(fieldData[offset]);

    //find max value
    int sumInt = 0;
    double sumDouble = 0;
    for (int i = offset; i < count + offset; i++) {
        char* s = fieldData[i];

        //if type inconsistent, fail
        if (getType(s) != type) {
            /*
             *printf("ERROR: field not consistent type.\n");
             *return 0;
             */
            continue;
        }

        if (type == 0) { //int
            //get value and add to sum
            int value = toInt(s);
            sumInt += value;
        }
        else if (type == 1) { //double
            //get value and add to sum
            double value = toDouble(s);
            sumDouble += value;
        }
        else {//non-numeric (error)
            /*
             *printf("ERROR: the field is non-numeric.\n");
             *return 0;
             */
            continue;
        }
    }
    
    if (type == 0) {
        printf("%d\n", sumInt / count);
    }
    else {
        printf("%f\n", sumDouble / (double)count);
    }

    return 1;
}

// returns number of records printed
int records(_Bool header, char* field, char* value, struct parse_container* pc){
      char** all_field = get_field(pc, field);
      if(!all_field)return 0;
      int ret = 0;

      for(int i = header; i < pc->n_entries; ++i){
            if(strstr(all_field[i], value)){
                  puts(pc->entries[i].raw_ln); ++ret;
            }
      }

      free(all_field);

      return ret;
}
